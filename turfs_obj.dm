turf
	Allturf
		icon='rsc/turf.dmi'
		nero
			icon_state="nero"
			density=1
		verde
			icon_state="pavimento"
		muro
			icon_state="muro"
			density=1
		erba
			icon_state="erba"
			Entered(mob/M)
				SpawnPkmn(M, src.z)
		sentieroverticale
			icon_state="sentieroverticale"
		sentieroorizzontale
			icon_state="sentieroorizzontale"
		sentierocentro
			icon_state="sentierocentro"
		sentieroeast
		sentierocentro
obj
	Special
		var/strength
		var/user
		icon='rsc/Special.dmi'
		Fireball
			icon_state="fire"
		Waterball
			icon_state="water"
		Electric_Spear
			icon_state="electro"
		density=1
		Bump(mob/E)
			var/Damage = max(0, src.strength-E.DEF)
			view(E) << "[user] hit [E] for [Damage] dmg!"
			for(var/mob/Player/M in world)
				if (M.name == user)
					E.TakeDamage(Damage, M)
			del src
		Bump(turf/T)
			del src


