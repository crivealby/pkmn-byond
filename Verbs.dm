mob
	verb
		Attack()
			set category="Attacks"
			if(tempo>=1)
				flick("Attack",usr)
				for(var/mob/M in get_step(src, src.dir))
					var/Damage = max(0, src.ATK-M.DEF)
					view(M) << "[src] hit [M] for [Damage] dmg!"
					M.TakeDamage(Damage, src)
				tempo=0
				spawn(10)
				tempo=1
			return
mob/Player
	verb
		becomeOP()
			ATK = 1999
		piano()
			usr.Move(locate(1,1,2))

		Say(T as text)
			if (T=="")	return
			world <<"<B>[usr]</B>:  [T]"

		Level()
			usr.Exp+=100
			usr.LevelCheck()

		Save()
			src.SaveProc()

		Who()
			var/counter=0
			for(var/mob/Player/M in world)
				counter+=1
				usr<<"([M.Level])  [M]"
			usr<<"<b>[counter] Giocatori"

		Delete()
			src.DeleteProc()
			del src


		Fireball()
			set category="Attacks"
			if(tempo>=1)
				var/obj/Special/F=new/obj/Special/Fireball(usr.loc)
				walk(F,usr.dir)
				tempo=0
				spawn(50)
				tempo=1
				F.user = usr.name
				F.strength = usr.ATK*10
			else
				usr<<"Puoi sparare un colpo solo ogni 5 secondi!"
			return

		Electric_Spear()
			set category="Attacks"
			if(tempo>=1)
				var/obj/Special/E=new/obj/Special/Electric_Spear(usr.loc)
				walk(E,usr.dir)
				tempo=0
				spawn(50)
				tempo=1
				E.user = usr.name
				E.strength = usr.ATK*10
			else
				usr<<"Puoi sparare un colpo solo ogni 5 secondi!"
			return

		Waterball()
			set category="Attacks"
			set name="Cannon Flash"
			if(tempo>=1)
				var/obj/Special/W=new/obj/Special/Waterball(usr.loc)
				walk(W,usr.dir)
				tempo=0
				spawn(50)
				tempo=1
				W.user = usr.name
				W.strength = usr.ATK*10
			else
				usr<<"Puoi sparare un colpo solo ogni 5 secondi!"
			return

mob/Trainer
	verb
		Talk()
			set src in view(1)
			alert("Eccoti qua","Maestro")
			var/newmap=input("Vuoi iniziare la tua avventura?","START") in list("Si","No")
			switch(newmap)
				if("Si")
					alert("Ok","Maestro","Avanti")
					alert("Ti teletrasporterò su una nuova regione ricca di pokemon!","Maestro","OK")
					usr.loc=locate(8,1,2)
				if("No")
					alert("Sei un mona")
