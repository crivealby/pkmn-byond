mob
	proc
		LevelCheck()
			if(src.Exp>=src.Nexp)
				src.Exp=0
				src.Nexp+=100
				src.Level+=1

				src<<"Ora sei al [src.Level]"
				if(tipo=="ELETTRO")
					src.MaxHP+=rand(1,5)
					src.ATK+=3
					src.DEF+=1
				if(tipo=="FUOCO")
					src.MaxHP+=rand(1,10)
					src.ATK+=rand(1,2)
					src.DEF+=rand(1,2)
				if(tipo=="ACCIAIO")
					src.MaxHP+=rand(2,10)
					src.ATK+=1
					src.DEF+=2
				src.HP=src.MaxHP
	proc
		SaveProc()
			var/FileName="Players/[ckey(src.key)].sav"
			if(fexists(FileName))	fdel(FileName)
			var/savefile/F=new(FileName)
			F["Type"]<<src.tipo
			F["Level"]<<src.Level
			F["Exp"]<<src.Exp
			F["Nexp"]<<src.Nexp
			F["HP"]<<src.HP
			F["MaxHP"]<<src.MaxHP
			F["ATK"]<<src.ATK
			F["DEF"]<<src.DEF
			F["LastX"]<<src.x
			F["LastY"]<<src.y
			F["LastZ"]<<src.z
			src<<"Character Saved..."

		LoadProc()
			var/FileName="Players/[ckey(src.key)].sav"
			if(fexists(FileName))
				var/savefile/F=new(FileName)
				F["Type"]>>src.tipo
				F["Level"]>>src.Level
				F["Exp"]>>src.Exp
				F["Nexp"]>>src.Nexp
				F["HP"]>>src.HP
				F["MaxHP"]>>src.MaxHP
				F["ATK"]>>src.ATK
				F["DEF"]>>src.DEF
				src.loc=locate(F["LastX"],F["LastY"],F["LastZ"])
				src<<"Character Loaded..."
				return 1

		DeleteProc()
			var/FileName="Players/[ckey(src.key)].sav"
			if(fexists(FileName))
				var/savefile/F=new(FileName)
				fdel(FileName)
				src<<"Character Deleted..."
				return 1
mob/Player
	proc
		LoginProc()
			tempo=1
			if(src.LoadProc())
				world<<"[src] è ritornato"

				if(tipo=="FUOCO")
					icon='rsc/pkmn1.dmi'
					usr.verbs-=/mob/Player/verb/Electric_Spear
					usr.verbs-=/mob/Player/verb/Waterball
					tipo="FUOCO"
					usr.desc="Un pokemon di tipo fuoco che ama incendiare i suoi nemici"
				else if(tipo=="ELETTRO")
					icon='rsc/pkmn3.dmi'
					usr.verbs-=/mob/Player/verb/Waterball
					usr.verbs-=/mob/Player/verb/Fireball
					tipo="ELETTRO"
					usr.desc="Un pokemon di tipo elettro a cui si addice molto il detto:\nL'ATTACCO E' LA MIGLIOR DIFESA!!"

				else if(tipo=="ACCIAIO")
					icon='rsc/pkmn2.dmi'
					usr.verbs-=/mob/Player/verb/Electric_Spear
					usr.verbs-=/mob/Player/verb/Fireball
					tipo="ACCIAIO"
					usr.desc="Un pokemon di tipo acciaio che concentra tutta la sua potenza nella sua difesa metallica"
			else
				alert("Ora scegli il tuo pokemon!","Scelta Pkmn...","SCEGLI!\nAcciaio garantisce più difesa.\nElettro garantisce più attacco.\nFuoco è un tipo bilanciato.\n")
				var/char=input("Scegli il tipo!","TYPE") in list("fuoco","acciaio","elettro")
				switch(char)
					if("fuoco")
						icon='rsc/pkmn1.dmi'
						usr.verbs-=/mob/Player/verb/Electric_Spear
						usr.verbs-=/mob/Player/verb/Waterball
						tipo="FUOCO"
						ATK += 2
						DEF += 2
						usr.desc="Un pokemon di tipo fuoco che ama incendiare i suoi nemici"

					if("acciaio")
						icon='rsc/pkmn2.dmi'
						usr.verbs-=/mob/Player/verb/Electric_Spear
						usr.verbs-=/mob/Player/verb/Fireball
						tipo="ACCIAIO"
						ATK += 1
						DEF += 3
						usr.desc="Un pokemon di tipo acciaio che concentra tutta la sua potenza nella sua difesa metallica"
					if("elettro")
						icon='rsc/pkmn3.dmi'
						usr.verbs-=/mob/Player/verb/Waterball
						usr.verbs-=/mob/Player/verb/Fireball
						tipo="ELETTRO"
						ATK += 3
						DEF += 1
						usr.desc="Un pokemon di tipo elettro a cui si addice molto il detto:\nL'ATTACCO E' LA MIGLIOR DIFESA!!"
				src.loc=locate(3,4,1)
				spawn(10)
				src.SaveProc()
				client.show_verb_panel=1

turf/Allturf/erba
	proc
		SpawnPkmn(var/mob/M, var/z)
			if (prob(5) == 1)
				var/mob/Enemy/P = new(locate(M.x+1, M.y, M.z))
				P.icon_state = DecidePkmn(z)
				P.Level = pick(prob(5); usr.Level-2, prob(20); usr.Level-1, prob(50); usr.Level, prob(20); usr.Level+1, prob(5); usr.Level+2);
				if (P.Level < 1)
					P.Level = 1
				P.MaxHP += P.Level*10
				P.HP += P.Level*10
				P.ATK += P.Level
				P.DEF += P.Level
				P.Exp = P.Level*25
				P.Fight(M)

		DecidePkmn(z)
			if (z==2)
				var/a = pick(1,2,3)
				if (a==1)
					return "Oddish"
				if (a==2)
					return "Pikachu"
				if (a==3)
					return "Nidoran"
mob/Enemy
	proc
		Fight(var/mob/M)
			while(src)
				if(get_dist(src, M) <= 1)
					src.dir = get_dir(src,M)
					src.Attack()
				else
					step_to(src,M)
				sleep(rand(4,8))
mob
	proc
		TakeDamage(var/Damage, var/mob/M)
			src.HP -= Damage
			if (src.HP <= 0)
				if (src.client)
					world << "[M] killed [src]!"
					src.HP = src.MaxHP
					src.loc = locate(3,3,1)
				else
					world << "You killed [src] for [src.Exp] Exp"
					world << "[M.Exp]"
					M.Exp += src.Exp
					world << "[M.Exp]"
					M.LevelCheck()
					del src
